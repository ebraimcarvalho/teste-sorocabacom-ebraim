import React from 'react';
import './App.css';

import GlobalStyle from './styles/global';
import Header from './components/Header';
import Main from './components/Main';
import Mouse from './components/Mouse';
import Cards from './components/Cards';
import Form from './components/Form';
import Footer from './components/Footer';

function App() {
  return (
    <div>
      <GlobalStyle />
      <Header id="top" />
      <Main />
      <Mouse />
      <Cards />
      <Form />
      <Footer />
    </div>
  );
}

export default App;
