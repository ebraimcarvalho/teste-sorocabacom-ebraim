import React from 'react';

import { Container, GoToTop } from './styles';

function Footer() {
  return (
    <Container>
      <GoToTop>
        <a href="#top">/\</a>
      </GoToTop>
    </Container>
  );
}

export default Footer;