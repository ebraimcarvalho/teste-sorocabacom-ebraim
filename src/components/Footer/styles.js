import styled from 'styled-components';

export const Container = styled.div`
  background-color: #363636;
  display: flex;
  justify-content: flex-end;
  height: 250px;
  padding-top: 100px;
  padding-right: 40px;
  width: 100%;
`;

export const GoToTop = styled.div`
  align-items: center;
  background-color: #FFFFFF;
  border-radius: 50%;
  color: #363636;
  display: flex;
  font-size: 36px;
  justify-content: center;
  height: 90px;
  vertical-align: bottom;
  width: 90px;

  a {
    letter-spacing: -3px;
    padding: 20px;
  }
`;