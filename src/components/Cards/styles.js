import styled from 'styled-components';

export const Container = styled.div`
  background-color: rgba(54, 54, 54, 1);
  padding: 60px 100px 170px;

  @media (max-width: 1024px) {
    padding: 60px 50px 170px;
  }

  @media (max-width: 800px) {
    padding: 40px 50px 170px;
  }

  .slick-slide {
    padding-top: 180px;    
  }

  .slick-prev:before, .slick-next:before {
    font-size: 25px;
    color: rgba(255,255,255,0.6);
  }

  .slick-next {
    /* right: -20px; */
  }
`;