import React from 'react';
import "../../../node_modules/slick-carousel/slick/slick.css"; 
import "../../../node_modules/slick-carousel/slick/slick-theme.css";
// import Slider from 'infinite-react-carousel';
import Slider from "react-slick";
import { Container } from './styles';

import Card from '../Card';
import img1 from '../../assets/images/card1.png';
import img2 from '../../assets/images/card2.png';
import img3 from '../../assets/images/card3.png';

function Cards() {
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    initialSlide: 0,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      },
      {
        breakpoint: 800,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 2
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  };
  return (
    <Container>
      <Slider {...settings} className="carousel">
        <Card img={img1} text='A Camerata foi apenas os dois no início, e suas fileiras nunca foram destinadas a exceder um número a ser contado em uma mão.' />
        <Card img={img2} text='Red, uma jovem cantora, entrou em posse do Transistor. Sendo a poderosa espada falante. O grupo Possessores quer tanto ela quanto o Transistor e está perseguindo implacavelmente a sua procura.' />
        <Card img={img3} text='Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da Camerata.' />

        <Card img={img1} text='A Camerata foi apenas os dois no início, e suas fileiras nunca foram destinadas a exceder um número a ser contado em uma mão.' />
        <Card img={img2} text='Red, uma jovem cantora, entrou em posse do Transistor. Sendo a poderosa espada falante. O grupo Possessores quer tanto ela quanto o Transistor e está perseguindo implacavelmente a sua procura.' />
        <Card img={img3} text='Sybil é descrita pelo Transistor como sendo os "olhos e ouvidos" da Camerata.' />
      </Slider>

    </Container>
  );
}

export default Cards;