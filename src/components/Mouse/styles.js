import styled from 'styled-components';

export const Container = styled.div`
  width: 29px;
  height: 48px;
  background: rgba(255, 255, 255, 1) 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
  border-radius: 124px;
  margin: 0 auto;
  position: relative;
  bottom: -25px;

  span div {
    background-color: #222;
    border-radius: 10px;
    height: 8px;
    margin: 0 auto;
    width: 3px;
    position: relative;
    top: 7px;
  }

  .down {
    color: rgba(255, 255, 255, 1);
    display: block;
    margin-top: 5px;
    text-align: center;
    letter-spacing: -1px;
    position: relative;
    bottom: -40px;
  }
`;