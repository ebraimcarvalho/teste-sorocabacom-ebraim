import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  background-color: #fff;
  box-shadow: 0px 3px 6px rgba(255, 255, 255, 0.39);
  border-radius: 27px;
  padding-bottom: 20px;
  margin: 0 auto;
  max-width: 420px;
  width: 100%;

  .holder {
    position: relative;
    top: -80px;
  }

  .image {
    background-color: rgba(54, 54, 54, 1);
    border: 2px solid rgba(255, 255, 255, 1);
    border-radius: 50px;
    width: 95%;
    margin: 0 auto;
    position: relative;
    overflow: hidden;

    @media (max-width: 800px) {
    top: 0px;
  }
  }

  img {
    width: 105%;
    position: relative;
    top: 20px;

    @media (max-width: 800px) {
      top: 20px;
    }
  }

  p {
    color: #363636;
    font-size: 14px;
    height: 140px;
    line-height: 20px;
    letter-spacing: 0px;
    padding: 25px 20px 20px 30px;

    @media (max-width: 1024px) {
      padding: 15px;
    }

    @media (max-width: 800px) {
      height: 100px;
      padding-top: 30px;
      padding-left: 30px;
      padding-right: 25px;
    }

    @media (max-width: 400px) {
      height: 100px;
      padding-top: 10px;
      padding-left: 15px;
      padding-right: 10px;
    }
  }
`;