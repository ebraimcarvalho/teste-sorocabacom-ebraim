import React from 'react';

import { Container } from './styles';

function Card(props) {
  return (
    <Container>
      <div className="holder">
        <div className="image">
          <img src={props.img} alt=""/>
        </div>
        <p>{props.text}</p>
      </div>
    </Container>
  );
}

export default Card;