import React, {useState} from 'react';

import { Container, FormWrapper, Button } from './styles';

function Form() {
  const [form, setForm] = useState({
    name: '',
    email: '',
    message: '',
  })
  const [erro, setErro] = useState('');
  const [success, setSuccess] = useState('');

  const handleChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    })
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(checkInputs()) {
      console.log('send form data!')
    }
  }

  const checkInputs = () => {
    if(!form.name || !form.email || !form.message) {
      setErro('Favor preencher todos os campos!');
      setSuccess('');
      return;
    }
    if(!validateEmail()) {
      setErro('Favor preencher com um Email válido!');
      setSuccess('');
      return;
    }
    setErro('');
    setSuccess('Agradecemos seu contato! Retornaremos em breve!');
    return true;
  }

  const validateEmail = () => {
    let test = /^[\w+\d+._]+@[\w+\d+_+]+\.[\w+\d+._]{2,8}$/g
    return test.test(form.email)
  }

  return (
    <Container>
      <FormWrapper>
        <h3>FORMULÁRIO</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
        <form>
          <div className="input-group">
            <input type="text" name="name" placeholder="Nome" onChange={ handleChange} value={form.name} />
            <input type="text" name="email" placeholder="Email" onChange={ handleChange} value={form.email} />
          </div>
          <textarea name="message" id="textarea" cols="30" rows="10" placeholder="Mensagem" onChange={ handleChange} />
          {erro && <p className="erro">{erro}</p>}
          {success && <p className="success">{success}</p>}
          <Button type="submit" onClick={handleSubmit}>ENVIAR</Button>
        </form>
      </FormWrapper>
    </Container>
  )
}

export default Form;