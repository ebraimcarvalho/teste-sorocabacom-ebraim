import styled from 'styled-components';

export const Container = styled.div`
  background: transparent linear-gradient(143deg, #7DEDE2 0%, #58B790 100%) 0% 0% no-repeat padding-box;
  height: 680px;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const FormWrapper = styled.div`
  background-color: #fff;
  border-radius: 10px;
  box-shadow: 0px 0px 6px #0000004D;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 760px;
  margin: 0 auto;
  padding: 80px;
  padding-bottom: 50px;
  position: relative;
  top: 0px;
  width: 60%;
  min-width: 320px;

  @media(max-width: 1024px) {
    padding: 40px;
    text-align: center;
    width: 80%;
  }

  @media(max-width: 800px) {
    padding: 40px 20px;
    text-align: center;
    width: 95%;
  }

  @media(max-width: 360px) {
    padding: 40px 20px;
    text-align: center;
    width: 95%;
  }

  h3 {
    color: #63C7A9;
    font-family: 'Montserrat' sans-serif;
    font-size: 30px;
    letter-spacing: -0.88px;
    margin-bottom: 40px;
    text-align: center;
  }

  p {
    font-size: 18px;
    letter-spacing: 0px;
    color: #363636;
    margin: 0 auto;
    margin-bottom: 40px;
    text-align: left;
  }

  form {
    width: 80%;

    @media(max-width: 800px) {
      width: 90%;
    }

    .input-group {
      display: flex;
      flex-wrap: wrap;
      justify-content: space-between;
      margin-bottom: 30px;
      width: 100%;

      @media(max-width: 1024px) {
        flex-direction: column;
        margin-bottom: 20px;

        input {
          &:last-of-type {
            margin-bottom: 0px;
          }
        }
      }
    }

    input {
      border: 1px solid #363636;
      height: 48px;
      width: 249px;
      max-width: 49%;

      color: #363636;
      font-family: 'Open Sans',sans-serif;
      font-size: 16px;
      letter-spacing: 0px;
      padding: 5px 10px;
      text-transform: capitalize;

      @media(max-width: 1024px) {
        margin-bottom: 20px;
        max-width: 100%;
        width: 100%;
      }
    }

    input[name="email"] {
      text-transform: lowercase;

      &::placeholder {
        text-transform: capitalize;
      }
    }

    textarea {
      border: 1px solid #363636;
      font-family: 'Open Sans',sans-serif;
      font-size: 16px;
      letter-spacing: 0px;
      padding: 10px 10px;
      resize: none;
      width: 100%;
    }

    p {
      margin-top: 5px;
    }

    .erro {
      color: red;
    }

    .success {
      color: green;
    }
  }
`;

export const Button = styled.button`
  background-color: #63C7A9;
  border: none;
  cursor: pointer;
  height: 48px;
  margin-top: 30px;
  outline: none;
  width: 249px;

  font-family: 'Open Sans',sans-serif;
  font-size: 16px;
  letter-spacing: 0px;
  color: #FFFFFF;
  text-transform: uppercase;

  &:hover {
    opacity: 0.9;
  }
`;