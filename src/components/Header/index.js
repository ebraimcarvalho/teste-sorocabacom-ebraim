import React from 'react';

import { Wrapper } from './styles';
import iconHeader from '../../assets/icons/icon-header.png';

function Header() {
  return (
    <>
      <Wrapper>
        <img src={iconHeader} alt=""/>
        <h1>SuperGiantGames</h1>
      </Wrapper>
    </>
  )
}

export default Header;