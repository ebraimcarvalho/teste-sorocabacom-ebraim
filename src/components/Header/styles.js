import styled from 'styled-components';

export const Wrapper = styled.header`
  background: #363636;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100vw;
  height: 110px;

  img {
    margin-left: -20px;
    max-width: 100%;
  }

  h1 {
    font: Bold 20px/36px 'Montserrat', sans-serif;
    letter-spacing: 0.68px;
    color: #FFFFFF;
    text-transform: uppercase;
  }
`;
