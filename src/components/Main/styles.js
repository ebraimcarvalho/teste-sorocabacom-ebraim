import styled from 'styled-components';

import bgHero from '../../assets/images/bg-hero.png';
import bgCard from '../../assets/images/bg-degrade-card.png';

export const Wrapper = styled.div`
  background-image: url(${bgHero});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center center;
  margin-bottom: -48px;
  overflow: hidden;
  width: 100vw;
  /* height: 600px; */

  .petala1 {
    position: relative;
    display: flex;
    justify-content: center;
    top: -500px;
    left: -190px;
    margin-bottom: -200px;
  }

  .petala2 {
    position: relative;
    display: flex;
    justify-content: center;
    top: -500px;
    right: -160px;
    margin-bottom: -200px;
  }

  .petala3 {
    position: relative;
    display: flex;
    justify-content: center;
    top: -170px;
    right: -100px;
    margin-bottom: -66px;
  }
`;

export const CardBG = styled.div`
  display: flex;
  flex-direction: column;
  background-image: url(${bgCard});
  background-repeat: no-repeat;
  background-size: auto;
  background-position: center;
  margin: 0 auto;
  margin-top: 0px;
  padding: 20px 20px 100px;
  position: relative;
  width: 440px;
  max-width: 100%;

  h3 {
    color: #fff;
    font-family: 'Open Sans', sans-serif;
    font-weight: bold;
    font-size: 14px;
    letter-spacing: 0px;
    margin-bottom: 15px;
  }

  img {
    max-width: 100%;
    margin: 0 auto;
  }

  blockquote {
    color: rgba(240, 240, 242, 1);
    font-size: 18px;
    margin-top: 10px;
    text-align: center;
    text-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
  }
`;

export const Mouse = styled.div`
  width: 29px;
  height: 48px;
  background: rgba(255, 255, 255, 1) 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 6px rgba(0, 0, 0, 0.16);
  border-radius: 124px;
  margin: 0 auto;
  position: relative;
  bottom: -25px;

  span div {
    background-color: #222;
    border-radius: 10px;
    height: 8px;
    margin: 0 auto;
    width: 3px;
    position: relative;
    top: 7px;
  }

  .down {
    color: rgba(255, 255, 255, 1);
    display: block;
    margin-top: 5px;
    text-align: center;
    letter-spacing: -1px;
    position: relative;
    bottom: -40px;
  }
`;