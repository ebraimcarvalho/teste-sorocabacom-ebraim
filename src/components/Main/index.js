import React from 'react';

import { Wrapper, CardBG } from './styles';
import cardMain from '../../assets/images/card-main.png';
import petala1 from '../../assets/images/petala1.png';
import petala2 from '../../assets/images/petala2.png';
import petala3 from '../../assets/images/petala3.png';

function Main() {
  return (
    <Wrapper>
      <CardBG>
        <h3>TRANSISTOR - RED THE SINGER</h3>
        <img src={cardMain} alt=""/>
        <blockquote>"Olha, o que quer que você <br/> esteja pensando, me faça <br/>  um favor, não solte."</blockquote>
      </CardBG>
      <div className="petala1">
        <img src={petala1} alt=""/>
      </div>
      <div className="petala2">
        <img src={petala2} alt=""/>
      </div>
      <div className="petala3">
        <img src={petala3} alt="" className="petala3"/>
      </div>
    </Wrapper>
  );
}

export default Main;