import {createGlobalStyle} from 'styled-components';

const GlobalStyle = createGlobalStyle`

  * {
    margin: 0px;
    padding: 0px;
    box-sizing: border-box;
  }

  html {
    scroll-behavior: smooth;
  }

  body {
    font-family: 'Open Sans', sans-serif;
    max-width: 100%;
    overflow-x: hidden;
    &::-webkit-scrollbar {
      width: 0px;
    }
  }

  a {
    text-decoration: none;
    color: inherit;
  }
`;

export default GlobalStyle;